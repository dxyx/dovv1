// scroll()
setTimeout(function() { $("html,body").css({"overflow": "auto"});}, 9800);

// button()
setTimeout(function() {window.scrollTo({ top: 0, behavior: "smooth" });}, 700);
setTimeout(function() {window.scrollTo({ top: 0, behavior: "smooth" });}, 10500);

// popup()
setTimeout(function() { $("#sb_desk").slideUp("slow");}, 21000);
setTimeout(function() { $("#sb_mob").slideUp("slow");}, 17000);

// selfdestcrt()
setTimeout(function() { $("#sb_mob,#sb_desk").remove();}, 22000);